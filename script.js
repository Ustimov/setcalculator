var calc = (function() {
    "use strict";
    return {
        calculate: function () {
            var firstInput = document.getElementById("first");
            if (!firstInput.value) {
                alert("Введите первое множество")
                return;
            }
            var firstSet = firstInput.value.split(" ");

            var secondInput = document.getElementById("second");
            if (!secondInput.value) {
                alert("Введите второе множество")
                return;
            }
            var secondSet = secondInput.value.split(" ");

            var resultSet = firstSet.concat(secondSet);
            resultSet = resultSet.reduce(function(set, current) {
                if (!set) {
                    set = [];
                }
                var inSet = false;
                for (var i = 0; i < set.length; i++) {
                    if (current == set[i]) {
                        inSet = true;
                    }
                }
                if (!inSet) {
                    set.push(current);
                }
                return set;
            }, 0);

            var result = ""
            for (var i = 0; i < resultSet.length; i++) {
                result += resultSet[i] + " ";
            }

            var spanResult = document.getElementById("result");
            spanResult.innerHTML = result;
        }
    }
})();